import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChannelroomPage } from './channelroom.page';

describe('ChannelroomPage', () => {
  let component: ChannelroomPage;
  let fixture: ComponentFixture<ChannelroomPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelroomPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChannelroomPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
