import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChannelroomPageRoutingModule } from './channelroom-routing.module';
import { ChannelDetailsPage } from '../channel-details/channel-details.page';
import { LongPressModule } from 'ionic-long-press';

import { ChannelroomPage } from './channelroom.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChannelroomPageRoutingModule,
    LongPressModule
  ],
  //  entryComponents:[ChannelDetailsPage],
  declarations: [ChannelroomPage]
})
export class ChannelroomPageModule {}
