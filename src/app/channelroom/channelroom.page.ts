import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../shared/chat.service';
import { WebsocketService } from '../shared/websocket.service';
import * as moment from 'moment';
import { IonContent, ModalController } from '@ionic/angular';
import { ChannelDetailsPage } from '../channel-details/channel-details.page';

@Component({
  selector: 'app-channelroom',
  templateUrl: './channelroom.page.html',
  styleUrls: ['./channelroom.page.scss'],
})
export class ChannelroomPage implements OnInit {
  constructor(
    private router: Router,
    private chatService: ChatService,
    private webSocketService: WebsocketService,
    public modalController: ModalController
  ) {

    this.webSocketService.newChannelMessageReceived().subscribe(data => {
      this.channelmessageArray.push(data);
      this.content.scrollToBottom(0);
      this.isTyping = false;
    });
  }
  @ViewChild(IonContent, { static: true }) content: IonContent;


  public username: any;
  public channelBox: any;
  public email: any;
  public chatroom;
  public message: any;
  public currentUser;
  messageArray ;
  channelmessageArray ;
  public isTyping = false;
  sk: boolean;
  check = false;
  flag = 0;
  chat: any = [];
  channelName: any;
  users = [];
  Users ;
  channels;
  addUsers = [];
  addnew: any;
  admin: any;


  ngOnInit() {

    this.channels = this.router.getCurrentNavigation().extras.state.room;


    this.webSocketService.userJoinRoom()
    .subscribe(data => {
      this.channelmessageArray.push(data);
      this.content.scrollToBottom(0);

    });


    this.webSocketService.userLeftRoom()
    .subscribe(data => {

      this.channelmessageArray.push(data);
      this.content.scrollToBottom(0);

    });

    this.chatService.getUsers().subscribe(users => {
      this.Users = users;
    });
    this.Channels();
  }


  Channels() {

    this.flag = 1;
    this.sk = true;
    this.channelName = this.channels;

    this.chatService.getChatChannelChat(this.channelName).subscribe(messages => {
        this.chat = messages;

        this.users = this.chat.chatroom[0].users;
        this.admin = this.chat.chatroom[0].admin;

        this.Users.forEach(element => {

          if (!this.users.find(user => user == element.username)) {
            this.addUsers.push(element.username);
          }

        });

        if (this.chat.chatroom.length > 0) {
        this.channelmessageArray = this.chat.chatroom[0].messages;
        this.content.scrollToBottom(0);

        }

    });
  }

  sendChannelMessage() {
    if ( this.message !== undefined && this.message !== '') {
      const currentTime = moment().format('hh:mm a');
      this.webSocketService.sendChannelMessage({room: this.channels, user: this.chatService.getLoggedInUser(),
       message: this.message, time: currentTime});
      this.message = '';
    }
  }


  typing() {
    this.webSocketService.typing({room: this.chatroom, user: this.chatService.getLoggedInUser()});
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ChannelDetailsPage,
      animated: true,
      componentProps: {
        channelName: this.channelName
      }
    });
    return await modal.present();
  }

  


}
