import { Component, OnInit } from '@angular/core';
import { ChatService } from '../shared/chat.service';
import { WebsocketService } from '../shared/websocket.service';
import { NavigationExtras, Router } from '@angular/router';
import { element } from 'protractor';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  Users;
  public username: any;
  public channelBox: any;
  public email: any;
  public chatroom;
  public currentUser;
  messageArray;
  chatsArray;
  channelmessageArray;
  public isTyping = false;
  chat: any = [];
  channelName: any;
  users = [];
  chatnames1 = [];
  name;

  constructor(private chatService: ChatService,
    private webSocketService: WebsocketService,
    private router: Router) {
    this.username = this.webSocketService.getChats(localStorage.getItem('username'));
    this.webSocketService.getChats1().subscribe((data: any) => {
      this.chatsArray = data;
      this.chatsArray.forEach(element => {
        element.name = element.name.replace(localStorage.getItem('username'), '');
      });
    });

    // this.webSocketService.newMessageReceived

  }



  ngOnInit() {
    this.webSocketService.joinChat({
      username: localStorage.getItem('username')
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');
    location.reload();
    setTimeout(() => {
      console.log('Async operation has ended');

      event.target.complete();
    }, 1000);
  }

  sendData(data) {
    const navigationExtras: NavigationExtras = {
      state: {
        username: data,
      }
    };
    this.router.navigate(['/chatroom'], navigationExtras);
  }


}