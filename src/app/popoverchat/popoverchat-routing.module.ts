import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopoverchatPage } from './popoverchat.page';

const routes: Routes = [
  {
    path: '',
    component: PopoverchatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopoverchatPageRoutingModule {}
