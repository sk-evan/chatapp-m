import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PopoverchatPage } from './popoverchat.page';

describe('PopoverchatPage', () => {
  let component: PopoverchatPage;
  let fixture: ComponentFixture<PopoverchatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoverchatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PopoverchatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
