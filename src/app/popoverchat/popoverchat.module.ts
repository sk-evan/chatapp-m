import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopoverchatPageRoutingModule } from './popoverchat-routing.module';

import { PopoverchatPage } from './popoverchat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopoverchatPageRoutingModule
  ],
  declarations: [PopoverchatPage]
})
export class PopoverchatPageModule {}
