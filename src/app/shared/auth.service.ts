import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, from, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { WebsocketService } from './websocket.service';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private isAuthenticated = false;
  private token: any;
  private authStatusListner = new BehaviorSubject<boolean>(
    localStorage.getItem('isLoggedIn') === 'true'
  );
  private tokenTimer: any;
  private userId: string;
  private mainToken: any;
  public cookieValue: string;
  public userName: string;
  public headers = new HttpHeaders();
  log;

  constructor(private http: HttpClient, private router: Router, public toastController: ToastController,
              private webSocketService: WebsocketService) {}

  async RegisterSucces() {
    const toast = await this.toastController.create({
      message: 'Succesfully registered',
      duration: 3000
    });
    toast.present();
  }

  async RegisterFail() {
    const toast = await this.toastController.create({
      message: 'Username Already Exists',
      color: 'danger',
      duration: 3000
    });
    toast.present();
  }

  async LoginSucces() {
    const toast = await this.toastController.create({
      message: `Welcome ${this.userName}`,
      duration: 3000
    });
    toast.present();
  }

  async LoginFail() {
    const toast = await this.toastController.create({
      message: 'Check your Credentials',
      color: 'danger',
      duration: 3000
    });
    toast.present();
  }

  async Logout() {
    const toast = await this.toastController.create({
      message: 'Logged Out ! See You Soon',
      color: 'danger',
      duration: 3000
    });
    toast.present();
  }

  getToken() {
    return this.token;
  }
  getIsAuth() {
      const loggedin = JSON.parse(localStorage.getItem('isLoggedIn'));
      return loggedin;
  }
  getUserId() {
    return this.userId;
  }
  getUserName() {
    return this.userName;
  }

  getAuthStatusListner() {
    return this.authStatusListner.asObservable();
  }

  register(data) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('https://chatterbox-005.herokuapp.com/api/users', data).subscribe((res: any) => {
        this.log = res.user_already_signed_up;
        if (this.log == true) {
          this.authStatusListner.next(false);
          this.RegisterFail();
        } else {
          this.RegisterSucces();
          this.router.navigate(['login']);
        }
      },
      error => {}
    );
  }
  login(data) {
    this.http
      .post<{
        token: string;
        username: string;
        expiresIn: number;
        userId: string;
      }>('https://chatterbox-005.herokuapp.com/api/login', data)
      .subscribe(
        response => {
          localStorage.setItem('isLoggedIn', 'true');
          this.userName = response.username;
          localStorage.setItem('username', this.userName);
          const token = response.token;
          this.token = token;
          this.webSocketService.joinChat({
            username : this.userName
          });
          if (token) {
            const expiresInDuration = response.expiresIn;
            this.setAuthTime(expiresInDuration);
            this.isAuthenticated = true;
            this.userId = response.userId;
            this.authStatusListner.next(true);
            const now = new Date();
            const expirationDate = new Date(
              now.getTime() + expiresInDuration * 1000
            );

            this.saveAuthData(token, expirationDate, this.userId);
            this.authStatusListner.next(true);
            this.LoginSucces();
            this.router.navigate(['main']);
          }
        },
        error => {
          this.authStatusListner.next(false);
          this.LoginFail();
        }
      );
  }
  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return;
    }
    const now = new Date();
    const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
    if (expiresIn > 0) {
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.setAuthTime(expiresIn);
      this.authStatusListner.next(true);
    }
  }

  logout() {
    const userName = {
      user: this.userName
    };

    this.http
      .post('https://chatterbox-005.herokuapp.com/api/logout', userName)
      .subscribe(data => {
        console.log(data);
      });

    this.webSocketService.logout({
        username : userName
      });
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListner.next(false);
    this.userName = null;
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.userId = null;
    // this.toastr.success(`Logged Out Succesfull`, 'Logged Out');
    this.router.navigate(['/home']);
  }

  
  private saveAuthData(token: any, expirationDate: Date, userId: string) {
    this.mainToken = token;
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
    localStorage.setItem('userId', userId);
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('username');
    localStorage.removeItem('expiration');
    localStorage.removeItem('isLoggedIn');
  }
  private getAuthData() {
    const token: any = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    const userId = localStorage.getItem('userId');
    if (!token || !expirationDate) {
      return;
      // tslint:disable-next-line: align
    }
    return {
      // tslint:disable-next-line: object-literal-shorthand
      token: token,
      expirationDate: new Date(expirationDate),
      // tslint:disable-next-line: object-literal-shorthand
      userId: userId
    };
  }

  private setAuthTime(duration: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }
}
