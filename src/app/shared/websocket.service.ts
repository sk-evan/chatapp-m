import { ChatService } from '../shared/chat.service';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class WebsocketService {

  private socket;
  onlineUsers: any;
  private userOnline = new Subject<any>();
  online$ = this.userOnline.asObservable();

  constructor() {
    this.socket =  io('https://chatterbox-005.herokuapp.com');
    this.socket.on('Online_Users', data => {
      this.userOnline.next();
      this.onlineUsers = data;
      console.log(this.onlineUsers);
      
    });
  }

  joinChat(user) {
    this.socket.emit('User_Joined', user);
  }
  logout(user) {
    this.socket.emit('Left_Chat', user);
  }
  joinRoom(data) {
    this.socket.emit('join', data);
  }

  createChannel(data) {
    this.socket.emit('joinChannel', data);
  }

  sendMessage(data) {
    this.socket.emit('message', data);
  }
  sendChannelMessage(data) {
    this.socket.emit('channelmessage', data);
  }

  updateChannel(data) {
    this.socket.emit('updateChannel', data);
  }

  DeleteUsers(data) {
    this.socket.emit('deleteusers', data);
  }

  newMessageReceived() {
    const observable = new Observable<{ user: string, message: string, time: string}>(observer => {
      this.socket.on('new message', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  onOnline(id) {
    let flag = false;
    if (id != null) {
      this.onlineUsers.forEach(element => {
        if (element.username == id) {
          flag = true;
        }
      });
    }
    if (flag == false) { return false; }
    else { return true; }
  }
  newChannelMessageReceived() {
    const observable = new Observable<{ user: string, message: string, time: string}>(observer => {
      this.socket.on('new message1', (data) => {

        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  userJoinRoom() {
    const observable = new Observable<{ user: string, message: string, time: string }>(observer => {
        this.socket.on('join room', (data) => {
            observer.next(data);
        });
        return () => { this.socket.disconnect(); };
    });
    return observable;
}


userLeftRoom() {
  const observable = new Observable<{ user: string, message: string, time: string }>(observer => {
      this.socket.on('leave room', (data) => {
          observer.next(data);
      });
      return () => { this.socket.disconnect(); };
  });
  return observable;
}

  typing(data) {
    this.socket.emit('typing', data);
  }

  receivedTyping() {
    const observable = new Observable<{ message: string}>(observer => {
      this.socket.on('typing', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  getChats(data) {
    this.socket.emit('getchats', data);
  }

  getChats1() {
    const observable = new Observable<{}>(observer => {
      this.socket.on('gotchats', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }


  getOnlineUsers() {
    const observable = new Observable<{ username: string, scoket: string}>(observer => {
      this.socket.on('Online_Users', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  deletechatroommessage(data) {
    this.socket.emit('deleteSingleMessage', data);
  }

  delete_single_chatmessage() {
    const observable = new Observable<{ message: string}>(observer => {
      this.socket.on('delete_message', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }


}
