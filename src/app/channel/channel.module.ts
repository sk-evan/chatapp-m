import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
// import { PopoverComponentPage } from '../popover-component/popover-component.page';
import { ChannelPageRoutingModule } from './channel-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { ChannelPage } from './channel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChannelPageRoutingModule,
    NgSelectModule
  ],
  // entryComponents:[PopoverComponentPage],
  declarations: [ChannelPage,
    //  PopoverComponentPage
    ]
})
export class ChannelPageModule {}
