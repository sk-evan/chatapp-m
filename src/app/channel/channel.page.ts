import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { ChatService } from '../shared/chat.service';
import { WebsocketService } from '../shared/websocket.service';
import { NgForm } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
@Component({
  selector: 'app-channel',
  templateUrl: './channel.page.html',
  styleUrls: ['./channel.page.scss']
})
export class ChannelPage implements OnInit {
  sk = 0;
  message: string;
  name: string;
  public Users;
  public Checks;
  public rooms = [];
  selectedUsers: any[];
  chatroom: string;
  currentUser: string;

  constructor(
    private chatService: ChatService,
    private webSocketService: WebsocketService,
    private router: Router
    // public popoverController: PopoverController
    
  ) {
    this.currentUser = this.chatService.getLoggedInUser();
  }

  ngOnInit() {
    this.sk= 0;
    this.getDataofUsers();
    this.currentUser = this.chatService.getLoggedInUser();


  
  
  }

  doRefresh(event) {
    console.log('Begin async operation');
    location.reload();
    setTimeout(() => {
      console.log('Async operation has ended');

      event.target.complete();
    }, 1000);
  }





  addChannel() {
    if (this.sk == 0) {
      this.sk = 1;
    } else {
      this.sk = 0;
    }
  }

  getDataofUsers() {
    this.chatService.getUsers().subscribe(users => {
      this.Users = users;

      this.getChannelData();

      this.Checks = users;

      let i = 0;

      this.Checks.forEach(element => {
        if (element.username == this.currentUser) {
          this.Checks.splice(i, 1);
        }
        i++;
      });
    });
  }

  getChannelData() {
    this.Users.forEach(element => {
      if (element.username === this.chatService.getLoggedInUser()) {
        if(element.room != null){
        for (let i = 0; i < element.room.length; i++) {
          this.rooms.push(element.room[i]);
        }
      }
    }
    });
  }

  addUsers(data: NgForm) {
    if (data.invalid) {
      return true;
    }

    this.selectedUsers.push(this.currentUser);

    this.sk = 0;
    this.webSocketService.createChannel({
      admin: this.currentUser,
      room: data.value.channelname,
      users: this.selectedUsers
    });
    data.reset();
  }

  sendData(data) {
    const navigationExtras: NavigationExtras = {
      state: {
          room: data,
      }
  };
    this.router.navigate(['/channelroom'], navigationExtras);
  }


  // async presentPopover(ev: any) {
  //   const popover = await this.popoverController.create({
  //     component: PopoverComponentPage,
  //     backdropDismiss: true,
  //     event: ev,
  //     translucent: true,
  //     animated: true
  //   });
  //   return await popover.present();
  // }
}
