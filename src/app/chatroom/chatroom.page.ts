import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ChatService } from "../shared/chat.service";
import { WebsocketService } from "../shared/websocket.service";
import * as moment from "moment";
import { NgForm } from "@angular/forms";
import { Observable } from "rxjs";
import { IonContent } from "@ionic/angular";
declare var jQuery: any;

@Component({
  selector: "app-chatroom",
  templateUrl: "./chatroom.page.html",
  styleUrls: ["./chatroom.page.scss"]
})
export class ChatroomPage implements OnInit {
  protected interval: any;
  index: any;
  online: string;
  display: string;
  typ: string;

  constructor(
    private router: Router,
    private chatService: ChatService,
    private webSocketService: WebsocketService
  ) {
    this.webSocketService.newMessageReceived().subscribe(data => {
      this.messageArray.push(data);
      this.content.scrollToBottom(0);
    });

    this.webSocketService.delete_single_chatmessage().subscribe(res => {
      this.getMessages();
    });

    // this.webSocketService.receivedTyping().subscribe(bool => {
    //   console.log(bool);

    //   this.isTyping = bool.message;
    // });

    this.webSocketService.receivedTyping().subscribe(typing => {
      this.typ = typing.message;
      if (this.typ.length == 0) {
        if (this.online != null) {
          this.display = "online";
        }
      }
      if (this.typ.length != 0) {
        this.display = this.typ;
      }
    });
  }
  @ViewChild(IonContent, { static: true }) content: IonContent;
  data: any;
  public username: any;
  public channelBox: any;
  public email: any;
  public chatroom;
  public message: any;
  public currentUser;
  messageArray;
  channelmessageArray;
  public isTyping: string;
  sk: boolean;
  check = false;
  flag = 0;
  chat: any = [];
  channelName: any;
  users = [];
  Users;
  channels;
  addUsers = [];
  addnew: any;
  admin: any;
  counter = 0;
  copiedMessage: any;
  onlineusers: any;

  public progress = 0;

  // Interval function

  ngOnInit() {
    this.counter = 0;

    this.webSocketService.online$.subscribe(() => {
      this.checkOnline();
    });

    this.data = this.router.getCurrentNavigation().extras.state.username;
    this.chatService.getUsers().subscribe(users => {
      this.Users = users;
    });

    this.sk = true;
    this.username = this.data;
    console.log(this.username);

    this.chats();
    this.checkOnline();
  }

  sendMessage() {
    if (this.message !== undefined && this.message !== "") {
      const currentTime = moment().format("hh:mm a");
      this.webSocketService.sendMessage({
        room: this.chatroom,
        user: this.chatService.getLoggedInUser(),
        message: this.message,
        time: currentTime
      });
      this.message = "";
      const data = "";
      this.webSocketService.typing({ room: this.chatroom, message: data });
    }
  }

  typing() {
    const data = "typing...";
    this.webSocketService.typing({ room: this.chatroom, message: data });
  }

  onSearch(value: string): void {
    if (value.length == 0) {
      const data = "";
      this.webSocketService.typing({ room: this.chatroom, message: data });
    }
  }

  chats() {
    this.flag = 0;
    const currentUser = this.chatService.getLoggedInUser();

    if (this.data != undefined) {
      if (currentUser < this.username) {
        this.chatroom = currentUser.concat(this.username);
      } else {
        this.chatroom = this.username.concat(currentUser);
      }
    }
    this.webSocketService.joinRoom({
      user: this.chatService.getLoggedInUser(),
      room: this.chatroom
    });
    this.getMessages();
  }

  getMessages() {
    this.chatService.getChatRoomsChat(this.chatroom).subscribe(messages => {
      this.chat = messages;
      if (this.chat.chatroom.length > 0) {
        this.messageArray = this.chat.chatroom[0].messages;

        this.content.scrollToBottom(0);
      }
    });
  }

  onPress(item) {
    // document.getElementById("p2").style.class = "blue";
    this.startInterval();
    this.counter = 1;
  }

  onPressUp(item, index) {
    this.index = index;
    this.copiedMessage = item;

    this.stopInterval();
  }

  startInterval() {
    const self = this;
    this.interval = setInterval(function() {
      self.progress = self.progress + 1;
    }, 50);
  }
  stopInterval() {
    clearInterval(this.interval);
  }

  cancel() {
    this.counter = 0;
  }

  copy(data: string) {
    this.counter = 0;
    const selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = data;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand("copy");
    document.body.removeChild(selBox);
  }

  delete(index) {
    const data = {
      index, // scalar value
      chatroom: this.chatroom
    };

    this.webSocketService.deletechatroommessage(data);
    this.counter = 0;
  }

  checkOnline() {
    console.log(this.username);
    if (this.webSocketService.onOnline(this.username)) {
      this.online = "online";
      this.display = "online";
    } else {
      this.online = null;
      this.display = null;
    }
    console.log(this.display);
    
  }
  id(id: any) {
    throw new Error("Method not implemented.");
  }
}
