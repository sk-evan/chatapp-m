import { Component, OnInit, Input } from '@angular/core';
import { NavParams, AlertController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { ChatService } from '../shared/chat.service';
import { NgForm } from '@angular/forms';
import { WebsocketService } from '../shared/websocket.service';
import * as moment from 'moment';
import { LongPressModule } from 'ionic-long-press';


@Component({
  selector: 'app-channel-details',
  templateUrl: './channel-details.page.html',
  styleUrls: ['./channel-details.page.scss']
})
export class ChannelDetailsPage implements OnInit {
  loadedGoalList: any[];

  constructor(
    public modalController: ModalController,
    private chatService: ChatService,
    private webSocketService: WebsocketService,
    public alertController: AlertController
  ) {

    this.chatService.getUsers().subscribe(users => {
      this.Users = users;
    });

    this.webSocketService.userLeftRoom()
    .subscribe(data => {

      this.list();
    });

    this.webSocketService.userJoinRoom()
    .subscribe(data =>{
      this.list();
    })
  }

  chat: any = [];
  @Input() channelName: string;

  users = [];
  admin: any;
  Users;
  addUsers = [];
  searchunit = 0;
  public username: any;
  public channelBox: any;
  public email: any;
  public chatroom;
  public message: any;
  public currentUser;
  messageArray ;
  channelmessageArray ;
  public isTyping = false;
  sk = false;
  check = false;
  flag = 0;
  selectedUsers: any[];
  deletedUsers = [];
  addnew: any;
  islogin: any = [];
  textarea;
  addunit = 0;
  userFilter: any = [];
  item;


  ngOnInit() {
    this.searchunit = 0;
    this.addunit = 0;
    this.list();

    this.chatService.getUsers().subscribe(users => {
      this.Users = users;
    });

  }

  dismissModal() {
    this.modalController.dismiss();
  }

  list() {

    this.chatService.getUsers().subscribe(users => {
      this.Users = users;
    });
    this.chatService
      .getChatChannelChat(this.channelName)
      .subscribe(messages => {
        this.chat = messages;

        this.users = this.chat.chatroom[0].users;
        this.admin = this.chat.chatroom[0].admin;
        this.loadedGoalList = this.users;

        this.Users.forEach(element => {
          if (!this.users.find(user => user == element.username)) {
            this.addUsers.push(element.username);
          }
        });
      });

  }

  search() {
    this.searchunit = 1;
  }

  addNewUsers(data: NgForm) {
    this.addunit = 0;
    if (this.selectedUsers != undefined) {
    const currentTime = moment().format('hh:mm a');
    this.webSocketService.updateChannel({
      room: this.channelName,
      users: this.selectedUsers,
      time: currentTime
    });
    }
  }

  addMembers() {

    if ( this.addunit == 0) {
    this.addunit = 1;
    } else {
      this.addunit = 0;
    }

  }

  deleteMembers(data) {
    this.deletedUsers.push(data);
    const currentuser = localStorage.getItem('username');

    const currentTime = moment().format('hh:mm a');

    this.webSocketService.DeleteUsers({
      room: this.channelName,
      admin: currentuser,
      users: this.deletedUsers,
      time: currentTime
    });


}

async presentAlertConfirm(data) {
  const alert = await this.alertController.create({
    header: 'Confirm!',
    message: `<strong>Are you sure you want to delete the user from ${this.channelName} channel</strong>`,
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Okay',
        handler: () => {
          this.deleteMembers(data);
        }
      }
    ]
  });

  await alert.present();
}

setFilteredLocations(ev: any) {
  this.initializeItems();
  const searchTerm = ev.srcElement.value;
  if (!searchTerm) {
    return;
  }
  this.users= this.users.filter(currentGoal => {
    if (currentGoal && searchTerm) {
      if (currentGoal.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
        return true;
      }
      return false;
    }
  });
}
initializeItems(): void {
  this.users = this.loadedGoalList;
}

doSomething($event){
  alert('hello');
  console.log("Sdfgsdfg" ,$event);
  
}

}
