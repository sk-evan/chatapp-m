import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PopoverchannelPage } from './popoverchannel.page';

describe('PopoverchannelPage', () => {
  let component: PopoverchannelPage;
  let fixture: ComponentFixture<PopoverchannelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoverchannelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PopoverchannelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
