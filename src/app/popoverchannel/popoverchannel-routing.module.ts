import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopoverchannelPage } from './popoverchannel.page';

const routes: Routes = [
  {
    path: '',
    component: PopoverchannelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopoverchannelPageRoutingModule {}
