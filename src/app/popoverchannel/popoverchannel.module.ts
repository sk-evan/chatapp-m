import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopoverchannelPageRoutingModule } from './popoverchannel-routing.module';

import { PopoverchannelPage } from './popoverchannel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopoverchannelPageRoutingModule
  ],
  declarations: [PopoverchannelPage]
})
export class PopoverchannelPageModule {}
