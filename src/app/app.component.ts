import { Component, OnInit } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './shared/auth.service';
import { ChatService } from './shared/chat.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController,
    private authService: AuthService,
    private chatService: ChatService
  ) {
    this.initializeApp();
  }
  public selectedIndex = 0;
  public username;

  public appPages = [
    {
      title: 'Inbox',
      url: '/main',
      icon: 'paper-plane'
    },
    {
      title: `People`,
      url: '/chat',
      icon: 'people'
    },
    {
      title: 'Channels',
      url: '/channel',
      icon: 'chatbubbles'
    },
    {
      title: 'Trash',
      url: '/folder/Trash',
      icon: 'trash'
    },
    {
      title: 'Spam',
      url: '/folder/Spam',
      icon: 'warning'
    }
  ];
  public labels = ['Work', 'Travel', 'Reminders'];
  hideMe: boolean;


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
  


  usernName() {
    return localStorage.getItem('username');

  }

  hide() {
    if (this.hideMe !== true) {
    this.hideMe = true;
    } else {
    this.hideMe = false;
    }
  }

  onLogout() {
      this.menu.close('main-content');
      this.authService.logout();

  }

}
