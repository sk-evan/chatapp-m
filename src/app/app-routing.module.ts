import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./main/main.module').then( m => m.MainPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'chat',
    loadChildren: () => import('./chat/chat.module').then( m => m.ChatPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'chatroom',
    loadChildren: () => import('./chatroom/chatroom.module').then( m => m.ChatroomPageModule),

  },
  {
    path: 'channel',
    loadChildren: () => import('./channel/channel.module').then( m => m.ChannelPageModule)
  },
  {
    path: 'channelroom',
    loadChildren: () => import('./channelroom/channelroom.module').then( m => m.ChannelroomPageModule)
  },
  {
    path: 'channel-details',
    loadChildren: () => import('./channel-details/channel-details.module').then( m => m.ChannelDetailsPageModule)
  },
  {
    path: 'popoverchat',
    loadChildren: () => import('./popoverchat/popoverchat.module').then( m => m.PopoverchatPageModule)
  },
  {
    path: 'popoverchannel',
    loadChildren: () => import('./popoverchannel/popoverchannel.module').then( m => m.PopoverchannelPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
