import { Component, OnInit } from '@angular/core';
import { ChatService } from '../shared/chat.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  Users;
  online;
  constructor(public chatService: ChatService,
              public router: Router) { }

  ngOnInit() {
    this.chatService.getUsers().subscribe(users => {
      this.Users = users;
      this.online = this.Users;
      console.log(this.online[0].isLoging);

    });
    
  }

  doRefresh(event) {
    console.log('Begin async operation');
    location.reload();
    setTimeout(() => {
      console.log('Async operation has ended');

      event.target.complete();
    }, 1000);
  }

  getUser() {
    return this.chatService.getLoggedInUser();
  }

  sendData(data) {
    const navigationExtras: NavigationExtras = {
      state: {
          username: data,
      }
  };
    this.router.navigate(['/chatroom'], navigationExtras);
  }

}
